from django.contrib import admin   
from django.urls import path, include
from .views import Index, conf, delete

app_name = 'App7'

urlpatterns = [
    path('',Index, name = 'Index'),
    path('<int:pk>/', delete, name = 'delete'),
    path('confirmation/', conf, name = 'confirmation')
] 