from django.shortcuts import render, redirect
from .models import App7Model
from .forms import App7Form

def Index(request):
    if request.method=="POST":
        frm = App7Form(request.POST)
        if frm.is_valid():
            frm2 = App7Model()
            frm2.name = frm.cleaned_data["name"]
            frm2.message= frm.cleaned_data["message"]
            frm2.save()
        return redirect("/confirmation")
    frm = App7Form()
    frm2 = App7Model.objects.all()
    frm_dictio = {
        'frm':frm, #form
        'frm2':frm2 #model/isi formnya
    }
    return render(request,'Index.html',frm_dictio)

def conf(request):
    msg_input = App7Model.objects.last()
    conf_dictio = {
        'msg_input': msg_input}
    return render(request, 'Conf.html',conf_dictio)

def delete(request, pk):
    if request.method == "POST":
        frm = App7Form(request.POST)
        if frm.is_valid():
            frm2 = App7Model()
            frm2.name = frm.cleaned_data["name"]
            frm2.message = frm.cleaned_data["message"]
            frm2.save()
        return redirect("/confirmation")
    else:
        App7Model.objects.filter(pk = pk).delete()
        frm = App7Form()
        frm2 = App7Model.objects.all()
        msg_dictio ={
            'frm' : frm,
            'frm2': frm2
        }
        return render(request, 'Index.html', msg_dictio)

