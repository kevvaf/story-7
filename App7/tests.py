from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest
from .views import Index
from .models import App7Model


# Create your tests here.

class Story7Test(TestCase):
    def test_story7_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'Index.html')

    def test_story7_using_index_func(self):
        found = resolve(reverse('App7:Index'))
        self.assertEqual(found.func, Index)

    def test_story7_confirmation_url(self):
        response = self.client.get('/confirmation')
        self.assertEqual(response.status_code, 301)

    def test_story_7_can_save_an_event_POST_request(self):
        self.client.post('', data={'name':'test', 'message':'test'})
        hitungjumlah = App7Model.objects.all().count()
        self.assertEqual(hitungjumlah,1)

    def test_story_7_model_add_message(self):
        App7Model.objects.create(name = 'test', message='test')
        hitungjumlah = App7Model.objects.all().count()
        self.assertEqual(hitungjumlah, 1)

    def test_story_7_model_check_name(self):
        App7Model.objects.create(name = 'test', message='test')
        var_name = App7Model.objects.get(name='test')
        self.assertEqual(str(var_name), 'test')
        
    def test_form(self):
        response = self.client.post('/', follow=True, data={
            'name' : 'test',
            'message' : 'test'
        })
        self.assertEqual(App7Model.objects.count(), 1)


if __name__ == '__main__':
    unittest.main(warnings='ignore')

class App7Selenium(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.maximize_window()
        self.browser.implicitly_wait(20)
    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_landing_page(self):
        self.browser.get(self.live_server_url + '/')
        self.assertIn('Story7', self.browser.title)
        self.assertIn('Name', self.browser.page_source)
        time.sleep(3)

    def test_form(self):
        self.browser.get(self.live_server_url + '/')
        name = self.browser.find_element_by_name('name')
        time.sleep(3)
        status = self.browser.find_element_by_name('message')
        time.sleep(3)
        submit = self.browser.find_element_by_name('submit')
        test_name= 'test_name'
        test_status = 'test_status'
        name.send_keys(test_name)
        status.send_keys(test_status)
        time.sleep(3)
        submit.send_keys(Keys.RETURN)

        yes = self.browser.find_element_by_link_text('Yes')
        yes.click()
        time.sleep(3)
        self.browser.get(self.live_server_url + '/')
        self.assertEqual(1, App7Model.objects.count())
        self.assertIn(test_name, self.browser.page_source)
        self.assertIn(test_status, self.browser.page_source)

    def test_tampilin(self):
        self.test_form()
        self.browser.get(self.live_server_url + '/')
        self.assertIn('test_name', self.browser.page_source)
        self.assertIn('test_status', self.browser.page_source)
